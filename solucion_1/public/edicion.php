<?php
    include("conexion.php");
    $usuarios ="SELECT * FROM estudiante";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>panel de edicion</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="container-tablee container-table--edit">
        <div class="table-titlee">Datos del Registro </div>
        <div class="table-header">N°</div>
        <div class="table-header">Nombre</div>
        <div class="table-header">edad</div>
        <div class="table-header">editar</div>
        <?php $resultado= mysqli_query($connection, $usuarios);
        while($row=mysqli_fetch_assoc($resultado)) {?>
            <div class="table-item"><?php echo $row["id"];?></div>
            <div class="table-item"><?php echo $row["nombre"];?></div>
            <div class="table-item"><?php echo $row["edad"];?></div>
            <div class="table-item">
                <a href="actualizar.php?id=<?php echo $row["id"];?>" class="table--item--link">edit</a>
            </div>
            <?php } mysqli_free_result($resultado);?>
    </div>
    <a href="index.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="top"
    title="Tooltip on top">
      inicio
    </button></a>
    <a href="conexion.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="right"
    title="Tooltip on right">
     conexion
    </button></a>
    <a href="mostrar.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="bottom"
    title="Tooltip on bottom">
     mostrar datos
    </button></a>
    <a href="edicion.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="left"
    title="Tooltip on left">
      editar
    </button></a>
    </body>
    </html>