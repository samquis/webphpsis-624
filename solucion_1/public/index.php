<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>FORMULARIO DE REGISTRO</title>
    <link rel="stylesheet" href="estilos.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/a><body class="fondo-blur">
<br>
<br>
<nav class="navbar navbar-success bg-danger">
  <div class="container-fluid">
    <a class="navbar-info">HOLA AL SISTEMA QUE DESARROLLAMOS</a>
    <form class="d-flex">
      <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
  </div>
</nav>
<form action="registrar.php" method="post" >
  <div class="row mb-4">
    <div class="col">
      <div class="form-outline">
        <input type="int" name="id" id="form3Example1" class="form-control" />
        <div class="alert alert-primary" role="alert">
          ID CORRESPONDIENTE
          </div>
      </div>
    </div>
  </div>

  <div class="form-outline mb-4">
    <input type="text" name="nombre" id="form3Example3" class="form-control" />
    <div class="alert alert-primary" role="alert">
       NOMBRE COMPLETO
    </div>
  </div>

  <div class="form-outline mb-4">
    <input type="int" name="edad" id="form3Example4" class="form-control" />
<div class="alert alert-primary" role="alert">
  EDAD
</div>
  </div>
  <button type="submit" class="btn btn-primary btn-block mb-4">REGISTRARSE</button>
</form>
<a href="index.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="top" title="Tooltip on top">  inicio
</button></a>
<a href="conexion.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="right" title="Tool on rigth"> conexion
</button></a>
<a href="mostrar.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Tool on bottom"> mostrar datos
</button></a>
<a href="edicion.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="left" title="Toolti on left">  Editar
</button></a>
</body>
</html>