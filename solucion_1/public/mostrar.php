<?php
include("conexion.php");
$usuarios="SELECT * FROM estudiante";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="container-table">
        <div class="table-title">Datos del Registro <a href="edicion.php" class="table-edit">Edicion</a> </div>
        <div class="table-header">N°</div>
        <div class="table-header">Nombre</div>
        <div class="table-header">Edad</div>
        <?php $resultado= mysqli_query($connection, $usuarios);
        while($row=mysqli_fetch_assoc($resultado)){?>
            <div class="table-item"><?php echo $row["id"];?></div>
            <div class="table-item"><?php echo $row["nombre"];?></div>
            <div class="table-item"><?php echo $row["edad"];?></div>
            <?php } mysqli_free_result($resultado); ?>
    </div>
<a href="index.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="top" title="Tooltip on top">  inicio
</button></a>
<a href="conexion.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="right" title="Tool on rigth"> conexion
</button></a>
<a href="mostrar.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Tool on bottom"> mostrar datos
</button></a>
<a href="edicion.php"><button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="left" title="Toolti on left">  Editar
</button></a>
</body>
</html>